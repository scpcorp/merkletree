all: install

REBUILD:
	@touch debug*.go

dependencies:
	go get github.com/dvyukov/go-fuzz/go-fuzz@90825f39c90b713570ea0cc748b0987937ae6288
	go get github.com/dvyukov/go-fuzz/go-fuzz-build@90825f39c90b713570ea0cc748b0987937ae6288
	go get gitlab.com/NebulousLabs/fastrand
	go get gitlab.com/NebulousLabs/errors
	./install-linter.sh

install: REBUILD
	go install

# lint runs golangci-lint (which includes golint, a spellcheck of the codebase,
# and other linters), the custom analyzers, and also a markdown spellchecker.
lint:
	golangci-lint run --disable-all --enable=errcheck --enable=vet --enable=gofmt ./...

test: REBUILD
	go test -v -tags='debug' -timeout=600s

test-short: REBUILD
	go test -short -v -tags='debug' -timeout=6s

cover: REBUILD
	go test -coverprofile=coverage.out -v -race -tags='debug' ./...

fuzz: REBUILD
	go install -tags='debug gofuzz'
	go-fuzz-build gitlab.com/NebulousLabs/merkletree
	go-fuzz -bin=./merkletree-fuzz.zip -workdir=fuzz

.PHONY: all REBUILD dependencies install test test-short cover fuzz benchmark
