module gitlab.com/scpcorp/merkletree

go 1.13

require (
	github.com/stretchr/testify v1.7.0
	gitlab.com/NebulousLabs/errors v0.0.0-20171229012116-7ead97ef90b8
	gitlab.com/NebulousLabs/fastrand v0.0.0-20181126182046-603482d69e40
	golang.org/x/crypto v0.0.0-20200109152110-61a87790db17
)
